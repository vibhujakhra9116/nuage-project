from rest_framework import serializers
from django.shortcuts import get_object_or_404
from .models import User,IOU
from django.db.models import Sum
class UserSerializer(serializers.ModelSerializer):
    owes = serializers.SerializerMethodField()
    owed_by = serializers.SerializerMethodField()
    balance = serializers.SerializerMethodField()
    def get_owes(self,obj):
        owes_dict = {}
        ious_borrowers = IOU.objects.filter(borrower = obj)
        for iou in ious_borrowers:
            amount = 0
            if iou.lander.name in owes_dict.keys():
                amount = owes_dict[iou.lander.name]+iou.amount
            else:
                amount = iou.amount
            owes_dict.update({iou.lander.name:amount}) 
        return owes_dict
    def get_owed_by(self,obj):
        owed_by_dict = {}
        ious_borrower = IOU.objects.filter(lander = obj)
        for iou in ious_borrower:
            amount = 0
            if iou.borrower.name in owed_by_dict.keys():
                amount = owed_by_dict[iou.borrower.name]+iou.amount
            else:
                amount = iou.amount
            owed_by_dict.update({iou.borrower.name:amount}) 
        return owed_by_dict
    def get_balance(self,obj):
        total_borrows = IOU.objects.filter(borrower = obj).aggregate(Sum('amount'))
        total_given = IOU.objects.filter(lander = obj).aggregate(Sum('amount'))
        borrows = total_borrows['amount__sum'] if total_borrows['amount__sum'] else 0
        given = total_given['amount__sum'] if total_given['amount__sum'] else 0
        return str(given-borrows)
    class Meta:
        model = User
        fields = ['name','owes',"owed_by","balance"]
