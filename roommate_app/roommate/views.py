from django.shortcuts import render
from .models import User,IOU
from rest_framework import status,viewsets
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from .serializers import *



class AddUserViewSet(viewsets.ModelViewSet):

    def create(self,request):
        ''' add user'''
        try:
            data = request.data
            print(data)
            if 'user' not in data:
                raise Exception('user name required')
            data['name'] = data.pop('user')
            print(data)
            serializer = UserSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response({"success":True,"message":"Created sucessfully",
                            "user":serializer.data},
                            status=status.HTTP_201_CREATED)
        except Exception as e:
            print("Exception",e)
            return Response({"success":False,"message": "Somthing Went wrong!"},
                            status=status.HTTP_400_BAD_REQUEST )

class UserViewSet(viewsets.ModelViewSet):
    def list(self,request):
        try:
            data = request.data
            users = data.get('users')
            users_list = []
            
            if users:
                if not isinstance(users,list):
                    raise Exception('users name should be in array format')
                else:
                    try:
                        users_list = [user_obj for user_obj in [User.objects.get(name = user) for user in users]]
                    except User.DoesNotExist:
                        raise Exception('User name is incorrect')           
            else:
                users_list = User.objects.all().order_by('name')

            serializer = UserSerializer(users_list,many = True)
            return Response({"success":True,"users":serializer.data},
                                status=status.HTTP_200_OK)
        except Exception as e:
            print("Exception",e)
            return Response({"success":False,"message": "Somthing Went wrong!"},
                            status=status.HTTP_400_BAD_REQUEST )

class IOUViewSet(viewsets.ModelViewSet):
    def create(self,request):
        try:
            data = request.data
            amount = data.get('amount')
            lander = data.get('lander')
            borrower = data.get('borrower')
            if not all([amount,lander,borrower]):
                raise Exception('all fields are required')
            if lander==borrower:
                raise Exception('lander and borrower can"t be same')
            else:
                iou = IOU()
                iou.lander = get_object_or_404(User,name = lander)
                iou.borrower = get_object_or_404(User,name = borrower)
                iou.amount = amount
                iou.save()
                lander_serializer = UserSerializer(iou.lander)
                borrower_serializer = UserSerializer(iou.borrower)
            return Response({"success":True,"lander":lander_serializer.data,"borrower":borrower_serializer.data},
                                status=status.HTTP_200_OK)
        except Exception as e:
            print("Exception",e)
            return Response({"success":False,"message": "Somthing Went wrong!"},
                            status=status.HTTP_400_BAD_REQUEST )
