from rest_framework.routers import DefaultRouter
from .views import *

router=DefaultRouter()
router.register(r'add', AddUserViewSet, basename='user_add')
router.register(r'users', UserViewSet, basename='user_details')
router.register(r'iou', IOUViewSet, basename='IOU')

urlpatterns = router.urls