from django.db import models

# Create your models here.
class User(models.Model):
    name = models.CharField('User Name', max_length=100,unique=True,null=False)
    created_at = models.DateTimeField("Created Date", auto_now_add=True)
    updated_at = models.DateTimeField("Updated Date", auto_now=True)
    def __str__(self):
        return self.name
class IOU(models.Model):
    lander = models.ForeignKey(User, on_delete=models.CASCADE, null=False,   
                                    related_name="iou_lander")
    borrower = models.ForeignKey(User, on_delete=models.CASCADE, null=False,   
                                    related_name="iou_borrower")
    amount = models.FloatField(null=False)
    created_at = models.DateTimeField("Created Date", auto_now_add=True)
    updated_at = models.DateTimeField("Updated Date", auto_now=True)
    def __str__(self):
        return self.lander.name+"--->"+self.borrower.name
